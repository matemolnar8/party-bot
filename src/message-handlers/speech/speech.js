import googleTTS from 'google-tts-api';

import { sleep } from '../../utils/sleep.js';
import { MessageHandler } from '../message-handler.js';
import { pandaify } from '../memes/pandaify.js';
import { getVoiceChannel, playInChannel, scheduleDisconnect } from '../../utils/voice-channel.js';
import { AudioPlayer, createAudioResource } from '@discordjs/voice';
import { EventType, messageBus } from '../../utils/message-bus.js';

const MAX_LENGTH = 200;
const COMMAND_LANGUAGE_MAP = {
  speak: 'en',
  speakhu: 'hu',
  speakde: 'de',
  speakes: 'es',
  speakit: 'it',
  speakfi: 'fi',
  speakpanda: async (text) => ['hu', await pandaify(text)],
};

export class SpeechHandler extends MessageHandler {
  constructor(client, prefix) {
    super(client, prefix);
    this.audioPlayer = new AudioPlayer();
  }

  async handleMessage(command, msg) {
    const voiceChannel = getVoiceChannel(msg, this.getVoiceParam(msg));

    if (!voiceChannel) {
      return;
    }

    const input = this.trimCommand(command, msg);
    const valid = this.validateText(input, msg);

    if (!valid) {
      await msg.react('❌');
      return;
    }

    const [lang, text] =
      typeof COMMAND_LANGUAGE_MAP[command] === 'function'
        ? await COMMAND_LANGUAGE_MAP[command](input)
        : [COMMAND_LANGUAGE_MAP[command], input];

    const [speechUrl] = await Promise.all([
      googleTTS.getAudioUrl(text, {
        lang,
      }),
      sleep(500),
    ]);

    this.log(`Speech url: ${speechUrl}`);

    messageBus.emit(EventType.PauseYoutube);
    const finished = await playInChannel(createAudioResource(speechUrl), voiceChannel, this.audioPlayer);
    finished.subscribe(() => {
      scheduleDisconnect();
      messageBus.emit(EventType.UnpauseYoutube);
    });

    await msg.react('🗣');
  }

  validateText(text, msg) {
    if (!text.length) {
      msg.reply('Please specify text!');
      return false;
    }

    if (text.length > MAX_LENGTH) {
      msg.reply(`Text is too long, the maximum length is ${MAX_LENGTH} characters.`);
      return false;
    }

    return true;
  }

  get commands() {
    return Object.keys(COMMAND_LANGUAGE_MAP);
  }

  get friendlyName() {
    return 'Text-to-speech';
  }
}
