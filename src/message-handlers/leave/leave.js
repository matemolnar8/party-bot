import { MessageHandler } from '../message-handler.js';
import { disconnectFrom, getVoiceChannel } from '../../utils/voice-channel.js';
import { EventType, messageBus } from '../../utils/message-bus.js';

export class LeaveHandler extends MessageHandler {
  constructor(client, prefix) {
    super(client, prefix);
  }

  async handleMessage(command, msg) {
    const voiceChannel = getVoiceChannel(msg);

    if (!voiceChannel) {
      return;
    }

    disconnectFrom(voiceChannel.guildId);
    messageBus.emit(EventType.StopYoutube);
    await msg.react('👋');
  }

  get commands() {
    return ['leave'];
  }

  get friendlyName() {
    return 'Leave voice channel';
  }
}
