import { getConfig, getConfigSync } from '../../utils/config.js';
import { getVoiceChannel, playSoundFileInChannel, scheduleDisconnect } from '../../utils/voice-channel.js';
import { MessageHandler } from '../message-handler.js';
import { EventType, messageBus } from '../../utils/message-bus.js';
import { AudioPlayer } from '@discordjs/voice';

export class SoundHandler extends MessageHandler {
  constructor(client, prefix) {
    super(client, prefix);
    this.audioPlayer = new AudioPlayer();
  }

  async handleMessage(command, msg) {
    const voiceChannel = getVoiceChannel(msg, this.getVoiceParam(msg));

    if (!voiceChannel) {
      return;
    }

    const sounds = await getConfig('sounds');
    const sound = sounds[command];

    this.log(`Playing sound: '${sound}'`);

    messageBus.emit(EventType.PauseYoutube);
    const finished = await playSoundFileInChannel(sound, voiceChannel, this.audioPlayer);
    finished.subscribe(() => {
      scheduleDisconnect();
      messageBus.emit(EventType.UnpauseYoutube);
    });

    await msg.react('🔈');
  }

  get commands() {
    return Object.keys(getConfigSync('sounds'));
  }

  get friendlyName() {
    return 'Sounds';
  }
}
