import { MessageHandler } from '../message-handler.js';

export class PingHandler extends MessageHandler {
  handleMessage(command, msg) {
    msg.reply('pong');
  }

  get commands() {
    return ['ping'];
  }

  get friendlyName() {
    return 'Ping';
  }
}
