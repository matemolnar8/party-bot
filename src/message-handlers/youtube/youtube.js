import {
  audioPlayerState$,
  connectTo,
  getAudioPlayer,
  getVoiceChannel,
  pauseAndUnsubscribe,
  scheduleDisconnect,
  unpauseAndSubscribe,
} from '../../utils/voice-channel.js';
import { MessageHandler } from '../message-handler.js';
import search from 'youtube-search';
import { AudioPlayerStatus, createAudioResource } from '@discordjs/voice';
import { stream, video_basic_info } from 'play-dl';
import { EventType, messageBus } from '../../utils/message-bus.js';

const COMMAND_PLAY = 'play';
const COMMAND_STOP = 'stop';
const COMMAND_NEXT = 'next';
const COMMAND_QUEUE = 'queue';
const COMMAND_PAUSE = 'pause';
const COMMAND_UNPAUSE = 'unpause';
const URL_REGEX = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

export class YoutubeHandler extends MessageHandler {
  constructor(client, prefix) {
    super(client, prefix);
    this.queue = [];
    audioPlayerState$.subscribe(([_, newState]) => {
      if (newState.status === AudioPlayerStatus.Idle) {
        this.log('Player idle');
        if (this.nowPlaying) {
          this.onStreamFinished(this.nowPlaying);
        }
      }
    });

    messageBus.getEvent$(EventType.StopYoutube).subscribe(async () => {
      await this.stop();
    });

    messageBus.getEvent$(EventType.PauseYoutube).subscribe(() => {
      if (this.nowPlaying) {
        pauseAndUnsubscribe();
      }
    });

    messageBus.getEvent$(EventType.UnpauseYoutube).subscribe(() => {
      if (this.nowPlaying) {
        unpauseAndSubscribe();
      }
    });
  }

  async handleMessage(command, msg) {
    switch (command) {
      case COMMAND_PLAY:
        await this.play(msg);
        break;
      case COMMAND_STOP:
        await this.stop(msg);
        break;
      case COMMAND_NEXT:
        await this.next(msg);
        break;
      case COMMAND_QUEUE:
        this.printQueue(msg);
        break;
      case COMMAND_PAUSE:
        this.pause();
        break;
      case COMMAND_UNPAUSE:
        this.unpause();
        break;
    }
  }

  async play(msg) {
    const voice = getVoiceChannel(msg, this.getVoiceParam(msg));
    if (!voice) {
      return;
    }

    const input = this.trimCommand(COMMAND_PLAY, msg);
    let url = await this.getUrl(input);
    let replyWithPreview = false;

    if (!url) {
      url = await this.searchYoutube(input);

      if (!url) {
        await msg.react('❌');
        await msg.reply('Cannot find video!');
        return;
      }

      replyWithPreview = true;
    }

    const info = await video_basic_info(url);
    const trackInfo = this.getTrackInfo({ url, info }, null, replyWithPreview);
    let contentMsg = msg;

    if (replyWithPreview) {
      contentMsg = await msg.reply(this.nowPlaying ? `Added ${trackInfo} to the queue` : `Playing ${trackInfo}`);
    } else {
      await msg.reply(this.nowPlaying ? `Added ${trackInfo} to the queue` : `Playing ${trackInfo}`);
    }

    const queueItem = { url, msg, contentMsg, info };
    this.queue.push(queueItem);

    if (!this.nowPlaying) {
      await this.checkQueue();
    }
  }

  async stop(msg) {
    this.queue = [];
    await this.stopCurrent(msg);
  }

  async next(msg) {
    await this.stopCurrent(msg, '⏭');
  }

  printQueue(msg) {
    let reply = '';

    if (this.nowPlaying) {
      const trackInfo = this.getTrackInfo(this.nowPlaying, this.getRequestor(this.nowPlaying.msg));
      reply += `Playing in ${this.nowPlaying.voice.name}: ${trackInfo}\n\n`;
    }

    if (this.queue.length === 0) {
      reply += 'There are no YouTube URLs in the queue.';
    } else {
      reply += 'Currently queued tracks: \n';
      reply += this.queue.reduce((result, current, index) => {
        let item = `${index + 1}: ${this.getTrackInfo(current, this.getRequestor(current.msg))}`;
        return result + item + '\n';
      }, '');
    }
    msg.reply(reply);
  }

  getTrackInfo(track, requestor, preview) {
    const url = preview ? track.url : `<${track.url}>`;
    let item = `${track.info.video_details.title} - ${url}`;
    if (requestor) {
      item += `\nRequested by: ${requestor}`;
    }
    return item;
  }

  getRequestor(msg) {
    return msg.member ? msg.member.displayName : msg.author.username;
  }

  async stopCurrent(msg, reaction) {
    if (this.nowPlaying) {
      getAudioPlayer().stop(true);
      this.nowPlaying = null;
      if (msg) {
        await msg.react(reaction || '⏹');
      }
    } else {
      if (msg) {
        await msg.react('❌');
        await msg.reply('Not playing currently.');
      }
    }
  }

  pause() {
    getAudioPlayer().pause();
  }

  unpause() {
    getAudioPlayer().unpause();
  }

  async checkQueue() {
    if (this.queue.length > 0) {
      const dequeued = this.queue.shift();
      await this.startPlayback(dequeued);
    } else {
      scheduleDisconnect();
    }
  }

  async startPlayback({ url, msg, contentMsg, info }) {
    const voice = getVoiceChannel(msg, this.getVoiceParam(msg));
    if (!voice) {
      return;
    }

    this.log(`Playing from YouTube: ${url}`);

    const connection = connectTo(voice);
    connection.subscribe(getAudioPlayer());

    const playdlStream = await stream(url);
    const resource = createAudioResource(playdlStream.stream, { inputType: playdlStream.type });
    getAudioPlayer().play(resource);

    const reaction = await contentMsg.react('▶');
    this.nowPlaying = { url, msg, contentMsg, info, voice, reaction, playdlStream };
  }

  async onStreamFinished({ reaction, msg }) {
    if (msg.guild) {
      await reaction.remove();
    }

    this.nowPlaying = null;
    await this.checkQueue();
  }

  async getUrl(input) {
    const urlMatch = URL_REGEX.exec(input);
    if (urlMatch && urlMatch[0]) {
      return urlMatch[0];
    }

    return null;
  }

  async searchYoutube(input) {
    const opts = {
      maxResults: 10,
      key: process.env.DISCORD_YOUTUBE_API_KEY,
    };

    const result = await search(input, opts);
    if (result) {
      return result.results[0].link;
    } else {
      return null;
    }
  }

  get commands() {
    return [COMMAND_PLAY, COMMAND_STOP, COMMAND_NEXT, COMMAND_QUEUE, COMMAND_PAUSE, COMMAND_UNPAUSE];
  }

  get friendlyName() {
    return 'YouTube';
  }
}
