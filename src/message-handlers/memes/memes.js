import { MessageHandler } from '../message-handler.js';
import { pandaify } from './pandaify.js';

export class MemesHandler extends MessageHandler {
  async handleMessage(command, msg) {
    const original = this.trimCommand(command, msg);
    if (original.length) {
      msg.channel.send(await pandaify(original));
    } else {
      msg.reply('Please specify text!');
    }
  }

  get commands() {
    return ['pandaify'];
  }

  get friendlyName() {
    return 'Memes';
  }
}
