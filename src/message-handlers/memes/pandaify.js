import { getConfig } from '../../utils/config.js';

const consonantRegex = /(dz|dzs|gy|sz|ty|ly|zs|cs|ny|[qwrtzpsdfghjklyxcvbnm])/gim;

const accented = 'áéíóöőúüű';
const deaccented = 'aeiooouuu';

const deaccenter = [...accented].reduce(
  (acc, curr, index) => ({
    ...acc,
    [curr]: deaccented[index],
  }),
  {}
);

const pandaify = async (text) => {
  const trimmed = text.trim();
  const dict = await getConfig('dict-panda');

  return trimmed
    .split(' ')
    .map((word) => pandaifyWord(word, dict))
    .join(' ');
};

const endOf = (group) => group.index + group[0].length;

const pandaifyWord = (text, dict) => {
  if (dict[text]) {
    return dict[text];
  }

  const groups = [...text.matchAll(consonantRegex)];
  const parts = [];
  let previousGroup;
  groups.forEach((group) => {
    const before = text.substr(
      previousGroup ? endOf(previousGroup) : 0,
      previousGroup ? endOf(group) - endOf(previousGroup) - group[0].length : group.index
    );

    parts.push({ text: before });
    parts.push({ text: group[0], candidate: true });

    previousGroup = group;
  });

  if (previousGroup) {
    parts.push({
      text: text.substr(endOf(previousGroup), text.length - endOf(previousGroup)),
    });
  }

  return parts
    .filter((part) => part.text.length)
    .map((part, index, arr) => ({
      ...part,
      candidate: part.candidate && index !== 0 && index !== arr.length - 1,
    }))
    .map((part, _, arr) => {
      if (part.candidate) {
        const duplicate = arr.filter((item) => item.candidate).length === 1 || Math.random() > 0.5;
        return duplicate ? `${part.text}${part.text}` : part.text;
      }

      return part.text;
    })
    .map((text) => [...text].map((char) => deaccenter[char] || char).join(''))
    .map((text) => text.toLowerCase())
    .join('');
};

export { pandaify };
