import fetch from 'node-fetch';
import { getConfig } from '../../utils/config.js';
import { MessageHandler } from '../message-handler.js';

export class ConversationHandler extends MessageHandler {
  constructor(client, prefix) {
    super(client, prefix);
    this.conversationIds = {};
  }

  async handleMessage(command, msg) {
    const conversationId = await this.getConversationId(msg);
    const result = await this.statement(command, conversationId, msg);
    msg.reply(`${result.text}`);
  }

  async getConversationId(msg) {
    const id = msg.author.id;
    if (this.conversationIds[id]) {
      return this.conversationIds[id];
    }

    const conversationId = await this.createConversation();
    return (this.conversationIds[id] = conversationId);
  }

  async createConversation() {
    const config = await getConfig('conversation');
    const url = new URL(`https://${config.accountDomain}/api/conversation/v2/create`);
    const params = { kb: config.kb };
    Object.keys(params).forEach((key) => url.searchParams.append(key, params[key]));

    const result = await fetch(url);
    const json = await result.json();
    return json.id;
  }

  async statement(command, conversationId, msg) {
    const config = await getConfig('conversation');
    const url = new URL(`https://${config.accountDomain}/api/conversation/v1/statement`);
    const params = {
      id: conversationId,
      statement: this.trimCommand(command, msg),
    };
    Object.keys(params).forEach((key) => url.searchParams.append(key, params[key]));

    const result = await fetch(url);
    return result.json();
  }

  get commands() {
    return ['c'];
  }

  get friendlyName() {
    return 'Conversation';
  }
}
