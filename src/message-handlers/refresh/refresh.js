import { refreshConfig } from '../../utils/config.js';
import { MessageHandler } from '../message-handler.js';

export class RefreshHandler extends MessageHandler {
  constructor(client, prefix, messageRouter) {
    super(client, prefix);
    this.messageRouter = messageRouter;
  }

  async handleMessage(command, msg) {
    await refreshConfig();
    await this.messageRouter.refreshCommands();
    msg.reply('Refreshed');
  }

  get commands() {
    return ['refresh'];
  }

  get friendlyName() {
    return 'Refresh config';
  }
}
