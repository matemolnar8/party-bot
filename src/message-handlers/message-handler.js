const CHANNEL_REGEX = /--channel="([\w\s-]+)"/;

export class MessageHandler {
  constructor(client, prefix) {
    this.client = client;
    this.prefix = prefix;

    if (this.constructor === MessageHandler) {
      throw new Error("Abstract MessageHandler can't be instantiated.");
    }

    if (!this.handleMessage) {
      throw new Error('You must implement handleMessage!');
    }
  }

  get name() {
    return this.__proto__.constructor.name;
  }

  get commands() {
    throw new Error('You must implement commands getter!');
  }

  get friendlyName() {
    return this.name;
  }

  trimCommand(command, msg) {
    const length = this.prefix.length + command.length;
    const text = msg.content.substr(length, msg.content.length - length).trim();
    return text.replace(CHANNEL_REGEX, '');
  }

  getVoiceParam(msg) {
    const channelParam = msg.content.match(CHANNEL_REGEX);
    return channelParam && channelParam[1];
  }

  log(message) {
    console.log(`[${this.name}] ${message}`);
  }
}
