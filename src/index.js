import dotenv from 'dotenv';
import Discord, { Intents } from 'discord.js';

import { getConfig, refreshConfig } from './utils/config.js';
import { MessageRouter } from './message-router.js';

import { MemesHandler } from './message-handlers/memes/memes.js';
import { PingHandler } from './message-handlers/ping/ping.js';
import { SpeechHandler } from './message-handlers/speech/speech.js';
import { LeaveHandler } from './message-handlers/leave/leave.js';
import { RefreshHandler } from './message-handlers/refresh/refresh.js';
import { SoundHandler } from './message-handlers/sound/sound.js';
import { ConversationHandler } from './message-handlers/conversation/conversation.js';
import { setClient } from './utils/client.js';
import { YoutubeHandler } from './message-handlers/youtube/youtube.js';
import { systemMessage } from './utils/system-message.js';

dotenv.config();

const bootstrap = async () => {
  await refreshConfig();
  const isDev = process.env.DISCORD_DEV;
  const prefix = isDev ? '?' : '!';

  const intents = new Intents();
  intents.add(
    Intents.FLAGS.DIRECT_MESSAGES,
    Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_VOICE_STATES,
    Intents.FLAGS.GUILD_MESSAGES
  );
  const client = new Discord.Client({
    intents,
  });
  setClient(client);

  const messageRouter = new MessageRouter(prefix);

  messageRouter.registerHandler(new SoundHandler(client, prefix));
  messageRouter.registerHandler(new PingHandler(client, prefix));
  messageRouter.registerHandler(new MemesHandler(client, prefix));
  messageRouter.registerHandler(new SpeechHandler(client, prefix));
  messageRouter.registerHandler(new YoutubeHandler(client, prefix));
  messageRouter.registerHandler(new LeaveHandler(client, prefix));

  const conversationConfig = await getConfig('conversation');
  if (conversationConfig.enabled === 'true') {
    messageRouter.registerHandler(new ConversationHandler(client, prefix));
  }

  messageRouter.registerHandler(new RefreshHandler(client, prefix, messageRouter));

  client.on('ready', () => {
    console.log(`[client] Logged in as ${client.user.tag}!`);
  });

  client.on('messageCreate', async (msg) => {
    if (msg.author.bot) {
      return;
    }

    await messageRouter.handleMessage(msg);
  });

  await client.login(process.env.DISCORD_BOT_TOKEN);
};

process.on('unhandledRejection', (error) => {
  const msg = `[main] unhandledRejection
${error.message}
Stacktrace:
${error.stack}`;

  console.error(msg);
  systemMessage(msg);
});

bootstrap();
