import { systemMessage } from './utils/system-message.js';

export class MessageRouter {
  constructor(prefix) {
    this.prefix = prefix;
    this.handlerMap = {};
    this.handlers = [];
    this.log(`Initialized with prefix: ${prefix}`);
  }

  async handleMessage(msg) {
    if (!msg.content.startsWith(this.prefix)) {
      return;
    }

    const command = msg.content.split(' ')[0].split(this.prefix)[1];
    if (!command) {
      return;
    }

    if (command === 'commands') {
      msg.reply(`Available commands: ${this.availableCommands}`);
      return;
    }

    if (!this.handlerMap[command]) {
      this.log(`Unknown command: ${command}`);
      msg.reply(`Unknown command: ${command}`);
      return;
    }

    const handler = this.handlerMap[command];
    this.log(`Handling command: '${command}' with '${handler.name}'`);
    try {
      await handler.handleMessage(command, msg);
    } catch (e) {
      await msg.react('❌');
      await systemMessage(`${e.message}\nStack trace:\n${e.stack}`);
      this.log(`Error handling message: ${msg.content}`);
      console.error(e);
    }
  }

  registerHandler(handler) {
    this.handlers.push(handler);
    this.registerCommands(handler);

    this.log(`Registered handler: ${handler.name} for commands: [${handler.commands.join(',')}]`);
  }

  registerCommands(handler) {
    handler.commands.forEach((command) => {
      this.handlerMap[command] = handler;
    });
  }

  refreshCommands() {
    this.handlerMap = {};
    this.handlers.forEach((handler) => this.registerCommands(handler));
  }

  log(message) {
    console.log(`[MessageRouter] ${message}`);
  }

  get availableCommands() {
    const groups = this.handlers.map((handler) => ({ friendlyName: handler.friendlyName, commands: handler.commands }));
    return (
      '\n' +
      groups
        .map(
          (group) => `${group.friendlyName}: ${group.commands.map((command) => `${this.prefix}${command}`).join(', ')}`
        )
        .join('\n')
    );
  }
}
