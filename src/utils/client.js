let client;

export const setClient = (c) => {
  client = c;
};

export const getClient = () => client;
