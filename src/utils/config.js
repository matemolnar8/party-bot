import { readFile } from 'fs';
import { promisify } from 'util';

const readFilePromise = promisify(readFile);

let config;

export async function getConfig(key) {
  try {
    await readConfigFile();

    return config[key] || {};
  } catch (e) {
    console.error(e);
    return {};
  }
}

export function getConfigSync(key) {
  return (config && config[key]) || {};
}

export async function getConfigString(key) {
  try {
    await readConfigFile();

    return config[key] || null;
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function refreshConfig() {
  config = null;
  await readConfigFile();
}

export async function readConfigFile() {
  if (!config) {
    const file = await readFilePromise('./config.json', 'utf8');
    config = JSON.parse(file);
  }
}
