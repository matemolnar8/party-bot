import { join } from 'path';
import { getClient } from './client.js';
import {
  AudioPlayerStatus,
  createAudioPlayer,
  createAudioResource,
  getVoiceConnection,
  joinVoiceChannel,
} from '@discordjs/voice';
import { fromEvent, Observable } from 'rxjs';
import { promises } from 'fs';
import { F_OK } from 'constants';

const LEAVE_TIMEOUT = 5 * 60 * 1000; // 5 min
const SOUNDS_PATH = './sounds';

const audioPlayer = createAudioPlayer();
const log = (message) => console.log(`[VoiceChannel] ${message}`);

async function checkFileExists(file) {
  try {
    await promises.access(file, F_OK);
  } catch (e) {
    throw new Error(`File ${file} does not exist`);
  }
}

export const playSoundFileInChannel = async (sound, voiceChannel, audioPlayer) => {
  const path = join(SOUNDS_PATH, sound);
  await checkFileExists(path);

  return playInChannel(createAudioResource(path), voiceChannel, audioPlayer);
};

export const audioPlayerState$ = fromEvent(audioPlayer, 'stateChange');

export const playInChannel = async (sound, voiceChannel, customPlayer) => {
  clearDisconnect();
  log(`Joining channel: ${voiceChannel.name}, ID: ${voiceChannel.id}`);

  const connection = connectTo(voiceChannel);
  const player = customPlayer || audioPlayer;

  connection.subscribe(player);

  player.play(sound);

  return new Observable((subscriber) => {
    player.once(AudioPlayerStatus.Idle, () => {
      subscriber.next();
      subscriber.complete();
    });
  });
};

export const unpauseAndSubscribe = () => {
  const connection = getVoiceConnection(getClient().guilds.cache.first().id);

  if (connection) {
    clearDisconnect();
    connection.subscribe(audioPlayer);
    audioPlayer.unpause();
  }
};

export const pauseAndUnsubscribe = () => {
  const connection = getVoiceConnection(getClient().guilds.cache.first().id);

  if (connection) {
    audioPlayer.pause();
    connection.state.subscription?.unsubscribe();
  }
};

export const getVoiceChannel = (msg, channelName) => {
  const voiceChannel = findVoiceChannel(msg, channelName);

  if (voiceChannel) {
    return voiceChannel;
  }

  msg.react('❌');
  msg.reply('You must be in a voice channel!');
  return null;
};

const findVoiceChannel = (msg, channelName) => {
  const guild = getClient().guilds.cache.first();

  if (channelName) {
    const channel = guild.channels.cache.find((channel) => channel.name === channelName);

    if (channel) {
      return channel;
    } else {
      throw new Error('No such channel');
    }
  }

  if (msg.member) {
    const guild = getClient().guilds.cache.first();

    return guild.channels.cache
      .filter((channel) => channel.members.find((member) => member.user.id === msg.author.id))
      .find((channel) => channel.type === 'GUILD_VOICE');
  }
};

export const connectTo = (channel) => {
  const connection = getVoiceConnection(channel.guild.id);

  if (connection) {
    return connection;
  }

  return joinVoiceChannel({
    channelId: channel.id,
    guildId: channel.guild.id,
    adapterCreator: channel.guild.voiceAdapterCreator,
  });
};

export const disconnectFrom = (guildId) => {
  const connection = getVoiceConnection(guildId);
  if (connection) {
    connection.disconnect();
    connection.destroy();
  }
};

export const getAudioPlayer = () => {
  return audioPlayer;
};

let disconnectTimeout;

export const clearDisconnect = () => {
  if (disconnectTimeout) {
    clearTimeout(disconnectTimeout);
  }

  log(`Cleared disconnect`);
};

export const scheduleDisconnect = () => {
  if (disconnectTimeout) {
    clearTimeout(disconnectTimeout);
  }

  log(`Scheduling disconnect in ${LEAVE_TIMEOUT}`);

  disconnectTimeout = setTimeout(() => {
    log(`Disconnecting`);
    disconnectFrom(getClient().guilds.cache.first().id);
    disconnectTimeout = null;
  }, LEAVE_TIMEOUT);
};
