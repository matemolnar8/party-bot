import { getClient } from './client.js';

export async function systemMessage(msg) {
  const guild = getClient()?.guilds.cache.first();
  if (guild) {
    await guild.systemChannel.send(msg);
  }
}
