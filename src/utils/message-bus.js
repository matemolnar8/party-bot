import { filter, Subject } from 'rxjs';

export const EventType = {
  StopYoutube: 'stopYoutube',
  PauseYoutube: 'pauseYoutube',
  UnpauseYoutube: 'unpauseYoutube',
};

class MessageBus {
  constructor() {
    this.events = new Subject();
  }

  emit(event, params) {
    this.events.next([event, params]);
  }

  getEvent$(eventType) {
    return this.events.pipe(filter(([event, _]) => event === eventType));
  }
}

export const messageBus = new MessageBus();
