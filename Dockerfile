FROM node:16

WORKDIR /app

COPY package.json /app
COPY package-lock.json /app

ARG docker_tag
ENV DOCKER_TAG=$docker_tag

RUN npm ci

COPY . /app
RUN npm run lint

CMD ["npm", "start"]
